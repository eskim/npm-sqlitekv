var Promise = require("bluebird"),
  debug = require("debug")("sqlitekv"),
  SQLite3 = require("sqlite3").verbose();

class DB {
  constructor(opts = {}) {
    this.options = {
      ...{
        tableName: "items",
        dbPath: ":memory:"
      },
      ...opts
    };

    var self = this;
    this.db = new SQLite3.Database(this.options.dbPath);
    this.db = Promise.promisifyAll(this.db);
    var tbl = this.options.tableName;

    // self.stmt_get = this._prepare(`SELECT * FROM ${tbl} WHERE key=? LIMIT 1`);
    // self.stmt_insert = this._prepare(
    //   `INSERT INTO ${tbl} (key,value,ctime,mtime) VALUES (?,?,?,?)`
    // );
    // self.stmt_update = this._prepare(
    //   `UPDATE ${tbl} SET value=?, mtime=? WHERE key=?`
    // );
    // self.stmt_all = this._prepare(`SELECT * FROM ${tbl}`);
    // self.stmt_find = this._prepare(`SELECT * FROM ${tbl} WHERE key LIKE ?`);
    // self.stmt_delete = this._prepare(`DELETE FROM ${tbl} WHERE key = ?`);
    this.connected = this.init();
  }

  close() {
    if (this.db) this.db.close();
  }

  _prepare(sql) {
    var stmt = this.db.prepare(sql);
    return Promise.promisifyAll(stmt);
  }

  async _serialize(f) {
    return new Promise((resolve, reject) => {
      db.serialize(f, (err, x) => {
        if (err) {
          reject(err);
        } else {
          resolve(x);
        }
      });
    });
  }

  async init() {
    var tbl = this.options.tableName;
    await this.db.runAsync(
      "CREATE TABLE IF NOT EXISTS " +
        tbl +
        "(" +
        " key   TEXT PRIMARY KEY," +
        " value TEXT," +
        " ctime INTEGER," +
        " mtime INTEGER)"
    );
  }

  async getset(k, v) {
    let old = await this.get(k);
    await this.put(k, v);
    return old;
  }
  async keys(){
    await this.connected;
    var rows = await this.db.allAsync( `SELECT key FROM ${this.options.tableName}`, []);
    return rows.map(r => r.key);
  }
  async get(key) {
    await this.connected;
    var row = await this.db.getAsync(
      `SELECT * FROM ${this.options.tableName} WHERE key=? LIMIT 1`,
      [key]
    );
    if (row) {
      return JSON.parse(row.value);
    }
    return row;
  }

  async set(key, value) {
    return await this.put(key, value)
  }
  async delete(key) {
    await this.connected;
    return await this.db.runAsync(
      `DELETE FROM ${this.options.tableName} WHERE key=?`,
      [key]
    );

  }
  async exists(key) {
    await this.connected;
    var row = await this.db.getAsync(
      `SELECT count(*) cnt FROM ${this.options.tableName} WHERE key=? LIMIT 1`,
      [key]
    );
    debug('exists for %s : count %s', key, row.cnt)
    return row.cnt > 0;
  }

  async put(key, value) {
    await this.connected;
    var t = new Date().getTime();
    var value_p = JSON.stringify(value);
    var old = await this.exists(key);
    console.log(old);
    if (old) {
      return await this.db.runAsync(
        `UPDATE ${this.options.tableName} SET value=?, mtime=? WHERE key=?`,
        [value_p, t, key]
      );
    } else {
      return await this.db.runAsync(
        `INSERT INTO ${
          this.options.tableName
        } (key,value,ctime,mtime) VALUES (?,?,?,?)`,
        [key, value_p, t, t]
      );
    }

    // self.get(key, function(err, v) {
    //   if (err || v === undefined) {
    //     self.stmt_insert.run([key, value_p, t, t], callback);
    //   } else {
    //     self.stmt_update.run([value_p, t, key], callback);
    //   }
    // });
    // return self;
  }

  // kvs.prototype.delete = function (key, callback) {
  //   var self = this;
  //   self.stmt_delete.run([key], callback);
  // };

  // kvs.prototype._rows2obj = function (rows) {
  //   var r = {};
  //   for (var i in rows) {
  //     var row = rows[i];
  //     var key = row.key;
  //     r[key] = JSON.parse(row.value);
  //   }
  //   return r;
  // };

  // kvs.prototype.all = function (callback) {
  //   if (typeof(callback) != "function") return this;
  //   var self = this;
  //   self.stmt_all.all([], function (err, rows) {
  //     var r = self._rows2obj(rows);
  //     callback(err, r);
  //   });
  //   return self;
  // };

  // kvs.prototype.find = function (prefix, callback) {
  //   if (typeof(callback) != "function") return this;
  //   var self = this;
  //   self.stmt_find.all([prefix+"%"], function (err, rows) {
  //     var r = self._rows2obj(rows);
  //     callback(r);
  //   });
  //   return self;
  // };

  // open(dbpath, callback) {
  //   var db = new kvs();
  //   db._open(dbpath, callback);
  //   return db;
  // };
}

module.exports = DB;